# This is a non-notebook version of the code

import matplotlib.pyplot as plt

# Read file content.
with open("mesures.txt", encoding="utf-8") as f:
    raw_data = f.read()

# Convert french decimal separator to imperial decimal separator.
raw_data = raw_data.replace(",", ".")

# Split according to empty lines.
#
# First is header with column names.
#
# Then come the various data series.
header, *raw_series = raw_data.split("\n\n")
header = header.split("\t")

# Look at what we got.
print(header)
print(f"Found {len(raw_series)} raw data blocks")

# Convert raw data serie to numeric values.

LEN_COL = 1
EFFORT_COL = 4
TITLE_COL = 5

titles = []
series = []

for raw_serie in raw_series:
    serie = []
    for line in raw_serie.split("\n"):

        # Do not let empty lines bother us.
        if not line.strip():
            continue

        # Get and convert the relevant column values.
        values = line.split("\t")
        serie.append((float(values[LEN_COL]), float(values[EFFORT_COL])))

        # Make an attempt at finding the serie title.
        try:
            title = values[TITLE_COL]
            if title.startswith("Mesure"):
                titles.append(title)
        except IndexError:
            pass

    series.append(serie)


print(
    f"""
        {'\n\t'.join(titles)}
    
{series=}
"""
)


def draw_plot(title, serie):
    # Set data to expected format.
    lengthes = [x for x, y in serie]
    efforts = [y for x, y in serie]

    # Put relevant label on the graph.
    plt.title(title)
    plt.xlabel(header[LEN_COL] + " (± 0.9 cm)")
    plt.ylabel(header[EFFORT_COL] + " (± 2.0 N)")

    # Draw theoritical constant behaviour.
    plt.plot(
        [0, lengthes[-1]],
        [0, efforts[-1]],
        color="orange",
        label="Comportement linéaire théorique",
    )

    # Draw collected data.
    # Cf. https://stackoverflow.com/a/8409110.
    plt.plot(lengthes, efforts, linestyle=":", marker="+", label="Mesures")

    # Ensure (0, 0) is actually at bottom left of graph.
    plt.ylim(bottom=0)
    plt.xlim(left=0)

    # Fill under the curve to show the total effort work.
    plt.fill_between(lengthes, efforts, color="C0", alpha=0.2, label="Travail")

    # Trigger graph rendering.
    plt.legend()

    plt.savefig(f"plots/{title}.png")
    plt.show()


for i, (title, serie) in enumerate(zip(titles, series)):
    draw_plot(title, serie)


# Identify the relevant comparisons.
comparisons = [
    ("Variation de la longueur", (1, 2, 3)),
    ("Variation de la puissance", (1, 4, 5, 6)),
    ("Variation de la matière et de courbure", (3, 7, 8)),
]

# Make plots bigger.
plt.rcParams["figure.figsize"] = (8, 10)

for title, curves in comparisons:
    plt.title(title)
    plt.xlabel(header[LEN_COL])
    plt.ylabel(header[EFFORT_COL])

    # plt.figure().set_figheight(10)

    for curve in curves:
        # Set data to expected format.
        lengthes = [x for x, y in series[curve - 1]]
        efforts = [y for x, y in series[curve - 1]]

        # Draw collected data.
        # Cf. https://stackoverflow.com/a/8409110.
        plt.plot(
            lengthes,
            efforts,
            # linestyle=':',
            marker="+",
            label=titles[curve - 1],
        )

    # Ensure (0, 0) is actually at bottom left of graph.
    plt.ylim(bottom=0)
    plt.xlim(left=0)

    # Trigger graph rendering.
    plt.legend()

    plt.savefig(f"plots/comparison-{title}.png")
    plt.show()


def work_trapeze(serie):
    result = 0

    # First point is always 0 cm, 0 N.
    prev_length, prev_effort = 0, 0

    for length, effort in serie[1:]:
        # Use simple trapeze surface formula.
        result += (prev_effort + effort) * (length - prev_length) / 2
        prev_length, prev_effort = length, effort

    # Length is in cm, we want joules.
    return result / 100


def work_triangle(serie):
    length, effort = serie[-1]
    return length * effort / 2 / 100


for title, serie in zip(titles, series):
    print(
        f"{title:70} raw: {(work_triangle(serie)):.2f} J, fine: {work_trapeze(serie):.2f} J"
    )
